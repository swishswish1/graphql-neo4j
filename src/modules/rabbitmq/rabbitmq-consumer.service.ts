import { Injectable, Logger, LoggerService } from '@nestjs/common';
import { Consumer } from 'rabbitmq-provider/consumer';
import { ServiceInstanceId } from '../../common/service-instance-id.provider';
import { ConfigService } from 'config-lib';
import { logger } from '../../common/logger';

@Injectable()
export class RabbitmqConsumerService {
  consumer: Consumer;

  async createAndStartProcessing(fnConsume: Function) {
    this.consumer = await Consumer.createConsumer({
        connUrl: new ConfigService().get('RABBITMQ_URL'),
        queue: ServiceInstanceId.getId(),
        noAck: true,
      },
      logger,
      fnConsume);
  }
}
