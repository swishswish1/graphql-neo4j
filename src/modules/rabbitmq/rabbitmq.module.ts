import { Module } from '@nestjs/common';
import { RabbitmqConsumerService } from './rabbitmq-consumer.service';

@Module({
  providers: [RabbitmqConsumerService],
  exports: [RabbitmqConsumerService]
})
export class RabbitMQModule {}
