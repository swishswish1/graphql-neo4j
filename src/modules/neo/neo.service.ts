import { Db } from './neo.provider';
import {
  typeAffiliation,
  typeOrganization,
  typePerson,
  typeRelation,
  typeRole,
} from './person.types';
import { Args, Parent } from '@nestjs/graphql';
import { node, relation } from 'cypher-query-builder';
import { Inject, Injectable, Logger, LoggerService } from '@nestjs/common';
import { initInsertNeo } from './temp-neo-insert';
import { orEmpty, orMinusOne } from '../../common/utils';
import { ConfigService } from 'config-lib';
import { logger } from '../../common/logger';

@Injectable()
export class NeoService {
  private static alreadyInit = false;
  private readonly db: Db;

  constructor(configService: ConfigService) {
    this.db = new Db(`bolt://${configService.get('NEO4J_HOSTNAME')}`,
      { username: configService.get('NEO4J_USER'), password: configService.get('NEO4J_PASSWORD') });

    // TEMPORARY
    if (!NeoService.alreadyInit) {
      initInsertNeo(this.db, this);
      NeoService.alreadyInit = true;
    }
  }


  // Queries

  async allPersons(): Promise<any[]> {
    const conn = this.db
      .matchNode('persons', 'Person')
      .return([ { persons: [ typePerson ] } ]);

    let persons;
    try {
      persons = await conn.run();
    }
    catch (err) {
      logger.error(err);
    }

    return persons;
  }

  async affiliations(@Parent() person: any) {
    const { id } = person;
    const conn = this.db
      .match([node('affiliations', 'Affiliation'), relation('in', ':HAS'), node('person', 'Person')])
      .where({ 'person.id': id })
      .return([ { affiliations: [ typeAffiliation ] } ]);

    let affs;
    try {
      affs = await conn.run();
    }
    catch (err) {
      logger.error(err);
    }

    return affs;
  }

  async relations(@Parent() person: any) {
    const { id } = person;
    const conn = this.db
      .match([node('relations', 'Relation'), relation('in', ':HAS'), node('person', 'Person')])
      .where({ 'person.id': id })
      .return([ { relations: [ typeRelation ] } ]);

    let rels;
    try {
      rels = await conn.run();
    }
    catch (err) {
      logger.error(err);
    }

    return rels;
  }

  async personById(@Args('id') arg): Promise<any> {
    const id = typeof arg === 'string' ? arg : arg.p2;
    const conn = this.db.matchNode('persons', 'Person')
      .where({ 'persons.id': id })
      .return([ { persons: [ typePerson ] } ]);
    let persons;
    try {
      persons = await conn.run();
    }
    catch (err) {
      logger.error(err);
    }

    return persons[0];
  }

  async personBySurname(@Args('surname') surname: string): Promise<any> {
    const conn = this.db.matchNode('persons', 'Person')
      .where({ 'persons.surname': surname })
      .return([ { persons: [ typePerson ] } ]);
    let persons;
    try {
      persons = await conn.run();
    }
    catch (err) {
      logger.error(err);
    }

    return persons[0];
  }

  async allOrganizations(): Promise<any[]> {
    const conn = this.db.matchNode('organizations', 'Organization')
      .return([ { organizations: [ typeOrganization ] } ]);
    let organizations;
    try {
      organizations = await conn.run();
    }
    catch (err) {
      logger.error(err);
    }

    return organizations;
  }

  async organization(affiliation: any) {
    const { id } = affiliation;
    const org = (await this.db
      .match([node('organization', 'Organization'), relation('in',':HAS'), node('affiliation', 'Affiliation')])
      .where({ 'affiliation.id': id })
      .return([ { organization: [ typeOrganization ] } ])
      .run()) as any;
    return org[0];
  }

  async role(affiliation: any) {
    const { id } = affiliation;
    const role = (await this.db
      .match([node('role', 'Role'), relation('in',':HAS'), node('affiliation', 'Affiliation')])
      .where({ 'affiliation.id': id })
      .return([ { role: [ typeRole ] } ])
      .run()) as any;
    return role[0];
  }

  async parent(parent: any) {
    const { id } = parent;
    const conn = this.db
      .match([node('parent', 'Organization'), relation('in',':HAS'), node('organization', 'Organization')])
      .where({ 'parent.id': id })
      .return([ { organization: [ typeOrganization ] } ]);

    let parents = [];
    try {
      parents = await conn.run();
    }
    catch (err) {
      logger.error(err);
    }
    return parents[0];
  }


  // Mutations

  createPersons = async (inputPersons): Promise<any> =>
    await this.db.writeTransaction(async tx => {
      let message = '';
      for (const inputPerson of inputPersons) {
        if (await Db.alreadyExists(tx, inputPerson.id)) {
          // The node already exists
          message += `Node (${inputPerson.id}) already exists. `;
          throw message;
        }

        // The node doesn't exist
        const insertPerson =
          `MERGE (${inputPerson.id}:Person {id:"${inputPerson.id}", ` +
          `givenName:"${inputPerson.givenName}", ` +
          `surname:"${inputPerson.surname}", ` +
          `born:${orMinusOne(inputPerson.born)}, ` +
          `phone:"${orEmpty(inputPerson.phone)}", ` +
          `email:"${orEmpty(inputPerson.email)}", ` +
          `address:"${orEmpty(inputPerson.address)}"}) `;
        await tx.run(insertPerson);

        await this.addAffiliations(inputPerson, tx);
        await this.addRelations(inputPerson, tx);

        message += `Node (${inputPerson.id}) created. `;
      }

      return message;
    });


  // Helpers

  private addAffiliations = async (input, tx) => {
    if (input.affiliations && input.affiliations.length > 0)
      for (let item of input.affiliations)
        await tx.run(
          `MATCH (person) WHERE (person.id = "${input.id}") ` +
          `MATCH (org) WHERE (org.id = "${item.organizationId}") ` +
          `MATCH (role) WHERE (role.id = "${item.roleId}") ` +
          `MERGE (${item.id}:Affiliation {id:"${item.id}", since:${item.since}}) ` +
          `MERGE (${item.id})-[:HAS]->(org) ` +
          `MERGE (${item.id})-[:HAS]->(role) ` +
          `MERGE (person)-[:HAS]->(${item.id})`
      );
  }

  private addRelations = async (input, tx) => {
    if (input.relations && input.relations.length > 0)
      for (let item of input.relations)
        await tx.run(
          `MATCH (person) WHERE (person.id = "${input.id}") ` +
          `MATCH (p2:Person) WHERE (p2.id = "${item.p2Id}")` +
          `MERGE (${item.id}:Relation {id:"${item.id}", kind:"${item.kind}", since:${item.since}, notes:"${item.notes}", p1:person.id, p2:p2.id})` +
          `MERGE (${item.id})-[:HAS]->(person)` +
          `MERGE (${item.id})-[:HAS]->(p2)` +
          `MERGE (person)-[:HAS]->(${item.id})`
      );
  }
}
