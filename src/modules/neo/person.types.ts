export const typePerson = {
  id:           'id',
  givenName:    'givenName',
  surname:      'surname',
  born:         'born',
  phone:        'phone',
  email:        'email',
  address:      'address',
  affiliations: 'affiliations'
}

export const typeAffiliation = {
  id: 'id',
  since: 'since',
  organization: 'organization',
  role: 'role'
}

export const typeRelation = {
  id: 'id',
  p1: 'p1',
  p2: 'p2',
  kind: 'kind',
  since: 'since',
  notes: 'notes'
}

export const typeOrganization = {
  id: 'id',
  name: 'name',
  address: 'address',
  parent: 'parent'
}

export const typeRole = {
  id: 'id',
  name: 'name',
  description: 'description'
}


