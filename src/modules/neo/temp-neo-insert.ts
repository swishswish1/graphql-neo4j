// TEMPORARY
import { Db } from './neo.provider';
import { NeoService } from './neo.service';
import { delayMs } from '../../common/utils';
import { logger } from '../../common/logger';

export async function initInsertNeo(db: Db, neoPersonService: NeoService) {
  let isOK = true;
  try {
    // Empty database
    await db.emptyDb();

    // Creating new records
    const organizationsAndRoles =
      // Organizations
      'MERGE (o_iitk:Organization {id:"o_iitk", name:"IIT Kanpur", address:"Kalyanpur, Kanpur, UP"}) ' +
      'MERGE (o_iitd:Organization {id:"o_iitd", name:"IIT Delhi", address:"Hauz Khas, Delhi"}) ' +
      'MERGE (o_iitk_me:Organization {id:"o_iitk_me", name:"Dept. ME, IIT/K", address:"Faculty Building, 4th floor"}) ' +
      'MERGE (o_iitk_cs:Organization {id:"o_iitk_cs", name:"Dept. CS, IIT/K", address:"Computer Centre"}) ' +

      // Parents
      'MERGE (o_iitk)-[:HAS]->(o_iitk_me) ' +
      'MERGE (o_iitk)-[:HAS]->(o_iitk_cs) ' +

      // Roles
      'MERGE (r_st:Role {id:"r_st", name:"Student", description:"Subject to brain wash"}) ' +
      'MERGE (r_pr:Role {id:"r_pr", name:"Professor", description:"Brain washer"}) ' +
      'MERGE (r_vpr:Role {id:"r_vpr", name:"Visiting Professor", description:"Temporary brain washer"}) ' +
      'MERGE (r_dir:Role {id:"r_dir", name:"Director", description:"Big Brother"}) ';

    await db.raw(organizationsAndRoles).run();
  }
  catch (err) {
    logger.error(err);
    isOK = false;
  }

  if (isOK) {
    await delayMs(1000);
    await neoPersonService.createPersons([
      {
        id: 'p_1',
        givenName: 'Praveen',
        surname: 'Srivastava',
        born: 2000,
        phone: '123-456-700',
        email: 'praveens@iitk.ac.in',
        address: 'qwwwqeqeqwe',
        affiliations: [
          {
            id: 'a_1',
            organizationId: 'o_iitk_me',
            roleId: 'r_st',
            since: 2018
          }
        ]
      },
      {
        id: 'p_2',
        givenName: 'Sangeeta',
        surname: 'Kohli',
        born: 1990,
        phone: '456-543-543',
        email: 'sanghitak@iitd.ac.in',
        address: 'rwerewrwrw',
        affiliations: [
          {
            id: 'a_2',
            organizationId: 'o_iitd',
            roleId: 'r_pr',
            since: 2019
          },
          {
            id: 'a_3',
            organizationId: 'o_iitk_me',
            roleId: 'r_vpr',
            since: 2017
          }
        ]
      },
      {
        id: 'p_3',
        givenName: 'Amitabha',
        surname: 'Mukherjee',
        born: 1985,
        phone: '321-346-377',
        email: 'amitabham@iitk.ac.in',
        address: 'rwerewrwrw',
        affiliations: [
          {
            id: 'a_4',
            organizationId: 'o_iitk_cs',
            roleId: 'r_pr',
            since: 2016
          },
          {
            id: 'a_5',
            organizationId: 'o_iitk_me',
            roleId: 'r_pr',
            since: 2015
          }
        ]
      }
    ]);
  }
}



