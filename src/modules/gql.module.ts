// Template for this file was automatically generated according to graphQL schema.
// The following block is subject to replacement on module runtime upload
// and should be maintained unchanged (including blanks).

// {
const isFromWeb = false;
let typePaths = [];
let path = '';
__dirname = '';
// }

if (isFromWeb)
  process.chdir(__dirname);

import { SqlConfig } from './sql/sql-config';
SqlConfig.setIsCalledFromGql(true);

import { Module, UseInterceptors, Logger } from '../../node_modules/@nestjs/common';
import {
  GraphQLModule,
  Resolver,
  Query,
  Args,
  ResolveField,
  Parent,
  Mutation
} from '../../node_modules/@nestjs/graphql';
import { ConfigService } from '../../node_modules/config-lib';
import { BaseResolver } from '../base-classes/base.resolver';
import { SqlModule } from './sql/sql.module';
import { NeoModule } from './neo/neo.module';
import { NeoService } from './neo/neo.service';
import { SqlService } from './sql/sql.service';
import { DurationInterceptor } from '../interceptors/duration.interceptor';
import { DirHolder } from '../modules-tools/dir-holder';

process.chdir(DirHolder.getProjectDir());

if (!isFromWeb)  {
  const configService = new ConfigService();
  const urlJoin = require('url-join');
  typePaths = [urlJoin(configService.get('GQL_URL'), configService.get('GQL_SCHEMA'))];
  path = configService.get('GQL_PATH');
}

@Resolver('Person')
export class PersonResolver extends BaseResolver<SqlService, NeoService> {
  constructor(sqlService: SqlService, neoService: NeoService) {
    super(sqlService, neoService);
  }

  // Args:   
  // Return: NonNullType,ListType,NonNullType,NamedType,Person
  @Query()
  @UseInterceptors(DurationInterceptor)
  async allPersons() {

    return await this.service.allPersons();
  }

  // Args:   {id: NonNullType,NamedType,String}
  // Return: NamedType,Person
  @Query()
  @UseInterceptors(DurationInterceptor)
  async personById(@Args('id') id: any) {

    return await this.service.personById(id);
  }

  // Args:   {surname: NonNullType,NamedType,String}
  // Return: NamedType,Person
  @Query()
  @UseInterceptors(DurationInterceptor)
  async personBySurname(@Args('surname') surname: any) {

    return await this.service.personBySurname(surname);
  }

  // Args:   {relationQueryArg: NonNullType,ListType,NamedType,RelationQueryArg}
  // Return: ListType,NamedType,Person
  @Query()
  @UseInterceptors(DurationInterceptor)
  async personsByRelation(@Args('relationQueryArg') relationQueryArg: any[]) {

    return [];  //@@
  }

  // Args:   {personsInput: NonNullType,ListType,NamedType,PersonInput}
  // Return: NamedType,String
  @Mutation()
  @UseInterceptors(DurationInterceptor)
  async createPersons(@Args('personsInput') personsInput: any[]): Promise<string> {

    return this.modifyDb(personsInput, this.sqlService.createPersons, this.neoService.createPersons);
  }

  // Args:   {organization: NamedType,String},
  //         {role: NamedType,String},
  //         {since: NamedType,IntInput}
  // Return: ListType,NamedType,Affiliation
  @ResolveField('affiliations')
  @UseInterceptors(DurationInterceptor)
  async affiliations(@Parent() parent: any,
                     @Args('organization') organization: any,
                     @Args('role') role: any,
                     @Args('since') since: any) {
    this.testPrint('affiliations, parent, organization, role, since ->', parent, organization, role, since); //TEST
    return await this.service.affiliations(parent);
  }

  // Args:   {kind: NamedType,String}
  // Return: ListType,NamedType,Relation
  @ResolveField('relations')
  @UseInterceptors(DurationInterceptor)
  async relations(@Parent() parent: any,
                  @Args('kind') kind: any) {
    this.testPrint('relations, parent, kind ->', parent, kind); //TEST
    return await this.service.relations(parent);
  }
}

@Resolver('Organization')
export class OrganizationResolver extends BaseResolver<SqlService, NeoService> {
  constructor(sqlService: SqlService, neoService: NeoService) {
    super(sqlService, neoService);
  }

  // Args:   
  // Return: NonNullType,ListType,NonNullType,NamedType,Organization
  @Query()
  @UseInterceptors(DurationInterceptor)
  async allOrganizations() {

    return await this.service.allOrganizations();
  }

  // Args:   
  // Return: NamedType,Organization
  @ResolveField('parent')
  @UseInterceptors(DurationInterceptor)
  async parent(@Parent() parent: any) {

    return await this.service.parent(parent);
  }
}

@Resolver('Affiliation')
export class AffiliationResolver extends BaseResolver<SqlService, NeoService> {
  constructor(sqlService: SqlService, neoService: NeoService) {
    super(sqlService, neoService);
  }

  // Args:   
  // Return: NonNullType,NamedType,Organization
  @ResolveField('organization')
  @UseInterceptors(DurationInterceptor)
  async organization(@Parent() parent: any) {

    return await this.service.organization(parent);
  }

  // Args:   
  // Return: NamedType,Role
  @ResolveField('role')
  @UseInterceptors(DurationInterceptor)
  async role(@Parent() parent: any) {

    return await this.service.role(parent);
  }
}

@Resolver('Relation')
export class RelationResolver extends BaseResolver<SqlService, NeoService> {
  constructor(sqlService: SqlService, neoService: NeoService) {
    super(sqlService, neoService);
  }

  // Args:   
  // Return: NonNullType,NamedType,Person
  @ResolveField('p1')
  @UseInterceptors(DurationInterceptor)
  async p1(@Parent() parent: any) {

    return [];  //@@
  }

  // Args:   
  // Return: NonNullType,NamedType,Person
  @ResolveField('p2')
  @UseInterceptors(DurationInterceptor)
  async p2(@Parent() parent: any) {

    return await this.service.personById(parent);
  }
}

@Module({
  imports: [
    GraphQLModule.forRoot({
      debug: false,
      playground: true,
      typePaths,
      path,
    }),
    SqlModule,
    NeoModule
  ],
  providers: [
    Logger,
    PersonResolver,
    OrganizationResolver,
    AffiliationResolver,
    RelationResolver
  ]
})
export class GqlModule { }

export function getModule() { return GqlModule; }
