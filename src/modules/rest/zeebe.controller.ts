// Note:
// file
// ...\node_modules\@nestjs\microservices\context\rpc-context-creator.js
// should be replaced with appropriate file from previous version provided in root directory.

import { Request, Response } from 'express';
import { Inject, Controller, Get, Post, Req, Res, UseInterceptors, Logger, LoggerService } from '@nestjs/common';
import { ZBClient } from 'zeebe-node';
import { ZEEBE_CONNECTION_PROVIDER, ZeebeWorker, ZeebeServer } from '@payk/nestjs-zeebe';
import { Consumer } from 'rabbitmq-provider/consumer';
import { ZeebeService } from './zeebe.service';
import { RabbitmqConsumerService } from '../rabbitmq/rabbitmq-consumer.service';
import { DurationInterceptor } from '../../interceptors/duration.interceptor';
import { BaseZeebeController } from '../../base-classes/base-zeebe.controller';
import { ConfigService } from 'config-lib';
import { logger } from '../../common/logger';
const { LogDurationInterceptor } = require('interceptors-lib');

@Controller()
export class ZeebeController extends BaseZeebeController {
  workflowInstance: any;

  constructor(private readonly configService: ConfigService,
              @Inject(ZEEBE_CONNECTION_PROVIDER) zbClient: ZBClient,
              zeebeServer: ZeebeServer,
              private zeebeService: ZeebeService,
              rabbitmqConsumerService: RabbitmqConsumerService // gateway consumer
  ) {
      super(zbClient, rabbitmqConsumerService,
        (consumer, messages) => {
          const payloads = Consumer.getPayloads(messages);
          logger.log(`@@@ send back events: ${JSON.stringify(messages)}`);
          payloads.forEach(item => this.sendResultToClient(item.sessionId, item.result));
        });
  }

  // Use the client to create a new workflow instance
  @Get('/1')
  @UseInterceptors(DurationInterceptor)
  @UseInterceptors(new LogDurationInterceptor(logger))
  async createWorkflow1(@Req() req: Request, @Res() res: Response) {
    logger.log('*** createWorkflow1');
    await this.createWorkflow(this.configService.get('ZEEBE_WORKFLOW_1'), res);
  }

  // ZeebeWorker 1

  // // Subscribe to events of type 'task-1-1' and
  // //   create a worker with the options as passed below (zeebe-node ZBWorkerOptions)
  // @ZeebeWorker('task-1-1')
  // async task11(job, complete) {
  //   await this.runTask(job, complete, this.zeebeService.task1);
  // }
  //
  // @ZeebeWorker('task-1-2')
  // async task12(job, complete) {
  //   await this.runTask(job, complete, this.zeebeService.task2, 3);
  // }

  // @ZeebeWorker('task-1-3')
  // async task13(job, complete) {
  //   await this.runTask(job, complete, this.zeebeService.task3, 1);
  // }
  //
  // @ZeebeWorker('task-1-4')
  // async task14(job, complete) {
  //   await this.runTask(job, complete, this.zeebeService.task4, 1);
  // }
}

