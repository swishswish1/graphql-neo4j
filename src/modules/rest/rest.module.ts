import { Inject, Module, Logger, LoggerService } from '@nestjs/common';
import { ZeebeService } from './zeebe.service';
import { ZBClient } from 'zeebe-node/dist';
import { ZEEBE_CONNECTION_PROVIDER, ZeebeModule, ZeebeServer } from '@payk/nestjs-zeebe/dist';
import { RabbitMQModule } from '../rabbitmq/rabbitmq.module';
import { ZeebeController } from './zeebe.controller';
import { SqlService } from '../sql/sql.service';
import { ConfigModule, ConfigService } from 'config-lib';
import { logger } from '../../common/logger';
import { SqlModule } from '../sql/sql.module';

@Module({
  imports: [
    ConfigModule,
    ZeebeModule.forRoot({
      gatewayAddress: new ConfigService().get('ZEEBE_GATEWAY_ADDRESS'),
      options: { loglevel: 'INFO', longPoll: 30000 },
    }),
    RabbitMQModule,
    SqlModule
  ],
  controllers: [ZeebeController],
  providers: [Logger, SqlService, ZeebeServer, ZeebeService],
  exports: [ZeebeService]
})
export class RestModule {
  constructor(configService: ConfigService,
              @Inject(ZEEBE_CONNECTION_PROVIDER) private readonly zbClient: ZBClient) {
    this.zbClient.deployWorkflow([
      `./bpmn/${configService.get('ZEEBE_WORKFLOW_1')}.bpmn`,
      `./bpmn/${configService.get('ZEEBE_WORKFLOW_2')}.bpmn`
    ])
    .then(res => {
      logger.log('Workflow deployed:');
      logger.log(res);
    });
  }
}
