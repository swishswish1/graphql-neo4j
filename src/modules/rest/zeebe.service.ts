import { Inject, Injectable, Logger, LoggerService } from '@nestjs/common';
import { logger } from '../../common/logger';

@Injectable()
export class ZeebeService {
  task1(job, complete) {
    logger.log('task-1 -> Task variables', job.variables);

    // Task worker business logic
    const result = '1';

    const variableUpdate = {
      tracer: 'task-1',
      status: 'ok',
      result,
    };

    complete.success(variableUpdate);
  }

  task2(job, complete, nextTask) {
    logger.log('task-2 -> Task variables', job.variables);

    // Task worker business logic
    const result = job.variables.result + '2';

    const variableUpdate = {
      tracer: 'task-2',
      status: 'ok',
      result,
      nextTask,
    };

    complete.success(variableUpdate);
  }

  task3(job, complete, wf): any {
    logger.log('task-3 -> Task variables', job.variables);

    // Task worker business logic
    const result = `workflow: <b><i>${wf}</i></b>  ${job.variables.result}.3... ${job.variables.sessionId}`;

    const variableUpdate = {
      tracer: 'task-3',
      status: 'ok',
      result,
    };

    complete.success(variableUpdate);
    return result;
  }

  task4(job, complete, wf): any {
    logger.log('task-4 -> Task variables', job.variables);

    // Task worker business logic
    const result = `workflow: <b><i>${wf}</i></b>  ${job.variables.result}.4... ${job.variables.sessionId}`;

    const variableUpdate = {
      tracer: 'task-4',
      status: 'ok',
      result,
    };

    complete.success(variableUpdate);
    return result;
  }
}
