import { Module, Logger } from '@nestjs/common';
import { ModulesHolder } from '../modules-tools/modules-holder';
import { ConfigModule } from 'config-lib';
import { RestModule } from './rest/rest.module';
import { isGqlFromWeb } from '../common/utils';

@Module({
  imports: [
    ConfigModule,
    isGqlFromWeb()
      ? ModulesHolder.getGqlModule()
      : require('./gql.module').GqlModule,
    RestModule
  ],
  providers: [Logger]
})
export class AppModule {}
