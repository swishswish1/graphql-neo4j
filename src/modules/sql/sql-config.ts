import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { ConfigService } from 'config-lib';
import { DirHolder } from '../../modules-tools/dir-holder';

export class SqlConfig {
  private static isCalledFromGql = false;

  static setIsCalledFromGql = (isCalledFromGql: boolean) =>
    SqlConfig.isCalledFromGql = isCalledFromGql;

  static getTypeOrmConfig = (): TypeOrmModuleOptions => {
    if (SqlConfig.isCalledFromGql)
      process.chdir(DirHolder.getProjectDir());

    const configService = new ConfigService();

    const retObj: TypeOrmModuleOptions = {
      type: 'mssql',
      host: configService.get('SQL_SERVER_HOST'),
      database: configService.get('SQL_SERVER_DATABASE'),
      entities: ['dist/modules/sql/entities/*.entity{.ts,.js}'],
      synchronize: true,
      autoLoadEntities: true,
      options: {
        enableArithAbort: true
      },
      username: configService.get('SQL_SERVER_USER'),
      password: configService.get('SQL_SERVER_PASSWORD')
    }

    if (SqlConfig.isCalledFromGql)
      process.chdir(DirHolder.getModulesDir());

    SqlConfig.isCalledFromGql = false;

    return retObj;
  }
}
