import { Connection } from 'typeorm';
import { Relation } from './entities/relation.entity';
import { Affiliation } from './entities/affiliation.entity';
import { Person } from './entities/person.entity';
import { Organization } from './entities/organization.entity';
import { Role } from './entities/role.entity';
import { Logger } from '@nestjs/common';

// TEMPORARY
export async function initInsertSql(connection: Connection) {
  const logger = new Logger();

  // Empty database
  try {
    await connection.getRepository(Relation).query('DELETE FROM relations2');
    await connection.getRepository(Affiliation).query('DELETE FROM affiliations');
    await connection.getRepository(Person).query('DELETE FROM persons');
    await connection.getRepository(Organization).query('DELETE FROM organizations');
    await connection.getRepository(Role).query('DELETE FROM roles');
  }
  catch (err) {
    logger.error(err);
  }

  const queryRunner = connection.createQueryRunner();
  await queryRunner.connect();
  await queryRunner.startTransaction();

  try {
    // Roles
    const roleStudent = new Role();
    roleStudent.id = 'r_st';
    roleStudent.name = 'Student';
    roleStudent.description = 'Subject for brain wash';

    const roleProfessor = new Role();
    roleProfessor.id = 'r_pr';
    roleProfessor.name = 'Professor';
    roleProfessor.description = 'Brain washer';

    const roleVisitingProfessor = new Role();
    roleVisitingProfessor.id = 'r_vpr';
    roleVisitingProfessor.name = 'Visiting Professor';
    roleVisitingProfessor.description = 'Temporary Brain washer';

    const roleDirector = new Role();
    roleDirector.id = 'r_dir';
    roleDirector.name = 'Director';
    roleDirector.description = 'Big Brother';

    await queryRunner.manager.save([roleStudent, roleProfessor, roleVisitingProfessor, roleDirector]);

    // Organizations
    const orgIIT_K = new Organization();
    orgIIT_K.id = 'o_iitk';
    orgIIT_K.name = 'IIT Kanpur';
    orgIIT_K.address = 'Kalyanpur, Kanpur, UP';

    const orgIIT_D = new Organization();
    orgIIT_D.id = 'o_iitd';
    orgIIT_D.name = 'IIT Delhi';
    orgIIT_D.address = 'Hauz Khas, Delhi';

    const orgIIT_K_ME = new Organization();
    orgIIT_K_ME.id = 'o_iitk_me';
    orgIIT_K_ME.name = 'Dept. ME, IIT/K';
    orgIIT_K_ME.address = 'Faculty Building, 4th Floor';

    const orgIIT_K_CS = new Organization();
    orgIIT_K_CS.id = 'o_iitk_cs';
    orgIIT_K_CS.name = 'Dept. CS, IIT/K';
    orgIIT_K_CS.address = 'Computer Centre';

    await queryRunner.manager.save([orgIIT_K, orgIIT_D, orgIIT_K_ME, orgIIT_K_CS]); //!

    orgIIT_K_ME.parent = orgIIT_K;
    orgIIT_K_CS.parent = orgIIT_K;

    await queryRunner.manager.save([orgIIT_K, orgIIT_D, orgIIT_K_ME, orgIIT_K_CS]); //!

    // Persons create
    const persons = new Array<Person>()
    for (let i = 0; i < 3; i++)
      persons[i] = new Person();

    // Persons fill
    persons[0].id = 'p_1';
    persons[0].givenName = 'Praveen';
    persons[0].surname = 'Srivastava';
    persons[0].born = 2000;
    persons[0].phone = '123-456-700';
    persons[0].email = 'praveens@iitk.ac.in';
    persons[0].address = 'qwwwqeqeqwe';

    persons[1].id = 'p_2';
    persons[1].givenName = 'Sangeeta';
    persons[1].surname = 'Kohli';
    persons[1].born = 1990;
    persons[1].phone = '456-543-543';
    persons[1].email = 'sanghitak@iitd.ac.in';
    persons[1].address = 'rwerewrwrw';

    persons[2].id = 'p_3';
    persons[2].givenName = 'Amitabha';
    persons[2].surname = 'Mukherjee';
    persons[2].born = 1985;
    persons[2].phone = '321-346-377';
    persons[2].email = 'amitabham@iitk.ac.in';
    persons[2].address = 'rwerewrwrw';

    await queryRunner.manager.save(persons);

    // Affiliations
    const aff1 = new Affiliation();
    aff1.id = 'a_1';
    aff1.organization = orgIIT_K_ME;
    aff1.role = roleStudent;
    aff1.since = 2018;
    aff1.person = persons[0];

    const aff2 = new Affiliation();
    aff2.id = 'a_2';
    aff2.organization = orgIIT_D;
    aff2.role = roleProfessor;
    aff2.since = 2019;
    aff2.person = persons[1];

    const aff3 = new Affiliation();
    aff3.id = 'a_3';
    aff3.organization = orgIIT_K_ME;
    aff3.role = roleVisitingProfessor;
    aff3.since = 2017;
    aff3.person = persons[1];

    const aff4 = new Affiliation();
    aff4.id = 'a_4';
    aff4.organization = orgIIT_K_CS;
    aff4.role = roleProfessor;
    aff4.since = 2016;
    aff4.person = persons[2];

    const aff5 = new Affiliation();
    aff5.id = 'a_5';
    aff5.organization = orgIIT_K_ME;
    aff5.role = roleProfessor;
    aff5.since = 2015;
    aff5.person = persons[2];

    await queryRunner.manager.save([aff1, aff2, aff3, aff4, aff5]);

    await queryRunner.commitTransaction();
  }
  catch (err) {
    await queryRunner.rollbackTransaction();
    logger.error(`Transaction rolled back: ${err}`);
  }
  finally {
    await queryRunner.release();
  }
}

