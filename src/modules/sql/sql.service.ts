import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { initInsertSql } from './temp-sql-insert';
import { SqlTransaction } from './sql-transaction';
import { Person } from './entities/person.entity';
import { Affiliation } from './entities/affiliation.entity';
import { Organization } from './entities/organization.entity';
import { Role } from './entities/role.entity';
import { Relation } from './entities/relation.entity';
import { logger } from '../../common/logger';

@Injectable()
export class SqlService extends SqlTransaction {
  private static alreadyInit = false;

  constructor(connection: Connection) {
    super(connection);

    // TEMPORARY
    if (!SqlService.alreadyInit) {
      initInsertSql(this.connection);
      SqlService.alreadyInit = true;
    }
  }

  // Queries

  allPersons = async (): Promise<Person[]> =>
    await this.connection.getRepository(Person).find();

  affiliations = async (person): Promise<Affiliation[]> =>
    await this.connection.getRepository(Affiliation).query(`SELECT * FROM affiliations WHERE person_id = ${person._id}`);

  relations = async (person): Promise<Relation[]> =>
    await this.connection.getRepository(Relation).query(`SELECT * FROM relations2 WHERE p1_id = ${person._id}`);

  organization = async (affiliation): Promise<Organization> =>
    (await this.connection.getRepository(Organization).query(`SELECT * FROM organizations WHERE _id = ${affiliation.organization_id}`))?.[0];

  role = async (affiliation): Promise<Role> =>
    (await this.connection.getRepository(Role).query(`SELECT * FROM roles WHERE _id = ${affiliation.role_id}`))?.[0];

  parent = async (organization): Promise<Organization> =>
    (await this.connection.getRepository(Organization).query(`SELECT * FROM organizations WHERE _id = ${organization.parent_id}`))?.[0];

  personById = async (arg): Promise<Person> => {
    const strWhere = typeof arg === 'string' ? `id = \'${arg}\'` : `_id = ${arg.p2_id}`
    return (await this.connection.getRepository(Person).query(`SELECT * FROM persons WHERE ${strWhere}`))?.[0];
  }

  personBySurname = async (surname: string): Promise<Person> =>
    (await this.connection.getRepository(Person).query(`SELECT * FROM persons WHERE surname = \'${surname}\'`))?.[0];

  allOrganizations = async (): Promise<Organization[]> =>
    await this.connection.getRepository(Organization).query('SELECT * FROM organizations');

  // Mutations

  organizationById = async (organizationId): Promise<Organization> =>
    (await this.connection.getRepository(Organization).query(`SELECT * FROM organizations WHERE id = \'${organizationId}\'`))?.[0];

  roleById = async (roleId): Promise<Role> =>
    (await this.connection.getRepository(Role).query(`SELECT * FROM roles WHERE id = \'${roleId}\'`))?.[0];

  createPersons = async (inputPersons): Promise<any> => {
    let isOK = true;
    let message = '';

    const queryRunner = await this.beginTransaction();

    try {
      for (const inputPerson of inputPersons) {
        const person = new Person();
        person.id = inputPerson.id;
        person.givenName = inputPerson.givenName;
        person.surname = inputPerson.surname;
        person.address = inputPerson.address;
        person.born = inputPerson.born;
        person.phone = inputPerson.phone;
        person.email = inputPerson.email;

        if (inputPerson.affiliations && inputPerson.affiliations.length > 0) {
          person.affiliations = new Array<Affiliation>();
          for (const aff of inputPerson.affiliations) {
            const affiliation = new Affiliation();
            affiliation.id = aff.id;
            affiliation.organization = await this.organizationById(aff.organizationId);
            affiliation.role = await this.roleById(aff.roleId);
            affiliation.since = aff.since;
            affiliation.person = person;
            person.affiliations.push(affiliation);
          }
        }

        if (inputPerson.relations && inputPerson.relations.length > 0) {
          person.relations = new Array<Relation>();
          for (const rel of inputPerson.relations) {
            const relation = new Relation();
            relation.id = rel.id;
            relation.p1 = person;
            relation.p2 = await this.personById(rel.p2Id);
            relation.kind = rel.kind;
            relation.since = rel?.since;
            relation.notes = rel?.notes;
            person.relations.push(relation);
          }
        }

        await queryRunner.manager.save(person);

        if (person.affiliations)
          await queryRunner.manager.save(person.affiliations);

        if (person.relations)
          await queryRunner.manager.save(person.relations);
      }
    }
    catch (err) {
      isOK = false;
      message = err.code === 'EREQUEST' ? 'Entries already exist in database. ' : `${err}`;
      logger.error(`Transaction rolled back: ${message}`);
    }

    return { queryRunner, isOK, message };
  }
}
