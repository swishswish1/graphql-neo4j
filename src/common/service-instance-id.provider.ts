import { Guid } from 'guid-typescript';

export class ServiceInstanceId {
  private static id = '';

  static getId() {
    if (ServiceInstanceId.id.length === 0)
      ServiceInstanceId.id = `${Guid.create()}`;

    return ServiceInstanceId.id;
  }
}
