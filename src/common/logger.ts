import * as winston from 'winston';
import { WinstonModule } from 'nest-winston';

const maxsize = 20480;
const maxFiles = 100;

const formatConsole = winston.format.combine(
  winston.format.timestamp(),
  winston.format.colorize(),
  winston.format.printf(info => `${info.timestamp}: ${info.level}: ${info.message}`)
);

const formatFile = winston.format.combine(
  winston.format.timestamp(),
  winston.format.printf(info => `${info.timestamp}: ${info.message}`)
);

export const logger = WinstonModule.createLogger({
  transports: [
    new winston.transports.Console({
      format: formatConsole
    }),
    new winston.transports.File({
      filename: 'log/log-info.log',
      level: 'info',
      format: formatFile,
      maxsize,
      maxFiles
    }),
    new winston.transports.File({
      filename: 'log/log-error.log',
      level: 'error',
      format: formatFile,
      maxsize,
      maxFiles
    })
  ]
});
