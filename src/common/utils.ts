import { ConfigService } from 'config-lib';

export const isGqlFromWeb = () => JSON.parse(new ConfigService().get('IS_GQL_FROM_WEB'));

export const fetchFromSQLServer = () => JSON.parse(new ConfigService().get('FETCH_FROM_SQL_SERVER'));

export const orEmpty = (obj) => obj ? obj : '';

export const orMinusOne = (obj) => obj ? obj : -1;

export function goOneDirUp(dir) {
  const delim = '/';
  const arr = dir.split(delim);
  let newDir = arr[0];
  for (let i = 1; i < arr.length - 1; i++)
    newDir += `${delim}${arr[i]}`;
  return newDir;
}

export function goNDirsUp(dir, n) {
  let currentDir = dir;
  for (let i = 0; i < n; i++)
    currentDir = goOneDirUp(currentDir);
  return currentDir;
}

export const delayMs = (duration: number): Promise<void> =>
  new Promise(resolve => setTimeout(() => resolve(), duration));


