import { NestFactory } from '@nestjs/core';
import { ModulesHolder } from './modules-tools/modules-holder';
import { ModuleLoader } from './modules-tools/module-loader';
import { isGqlFromWeb, fetchFromSQLServer } from './common/utils';
import { ZeebeServer } from '@payk/nestjs-zeebe/dist';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { logger } from './common/logger';

function getPort(app): number {
  const args = process.argv;
  return parseInt(args.length > 2 ? args[2] : app.get('ConfigService').getInt('PORT'));
}

const getUrl = async app => (await app.getUrl()).replace('[::1]', 'localhost');

(async function bootstrap() {
  logger.log('Main Service started...');
  logger.log(`GQLModule from Web = ${isGqlFromWeb()}`);

  const moduleLoader = new ModuleLoader('../modules');

  if (isGqlFromWeb())
    ModulesHolder.setGqlModule(await moduleLoader.loadModuleFromWeb('gql.module'));

  const app = await NestFactory.create(moduleLoader.loadModule('app.module').AppModule,
    { logger });

  // Zeebe workflow
  const microservice = app.connectMicroservice({
    strategy: app.get(ZeebeServer)
  });

  await app.startAllMicroservicesAsync();

  // OpenAPI (Swagger)
  const options = new DocumentBuilder()
    .setTitle('Persons example')
    .setDescription('Persons API description')
    .setVersion('1.0')
    .addTag('Persons')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api', app, document);

  await app.listen(getPort(app));

  const url = await getUrl(app);

  const fromWhichDbDataRead = fetchFromSQLServer() ? '\x1b[33mSQL Server\x1b[0m' : '\x1b[1m\x1b[32mNeo4j\x1b[0m' ;
  const message = `\nBrowse\n${url}/gql for GraphQL\n${url}/1 for Zeebe\n${url}/api for OpenAPI (Swagger)\n\n` +
                  `Data will be fetched from ${fromWhichDbDataRead} database.`;
  setTimeout(() => console.log(message), 500);
})();
