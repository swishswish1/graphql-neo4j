import { Injectable, Inject, NestInterceptor, ExecutionContext, CallHandler, LoggerService } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { logger } from '../common/logger';

@Injectable()
export class DurationInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const funcName = context.getHandler().name;
    logger.log(`*** Before ${funcName} ...`);
    const before = Date.now();
    return next.handle().pipe(
        tap(() => logger.log(`*** After ${funcName} ${Date.now() - before} ms`)));
  }
}
