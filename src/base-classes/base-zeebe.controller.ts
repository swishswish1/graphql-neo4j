import { Response } from 'express';
import { HttpStatus, Logger } from '@nestjs/common';
import { Guid } from 'guid-typescript';
import { ZBClient } from 'zeebe-node/dist';
import { ServiceInstanceId } from '../common/service-instance-id.provider';
import { RabbitmqConsumerService } from '../modules/rabbitmq/rabbitmq-consumer.service';
import { logger } from '../common/logger';

export class BaseZeebeController {
  responses = new Map<string, Response>();
  workflowInstance: any;

  constructor(protected zbClient: ZBClient,
              protected readonly rabbitmqConsumerService: RabbitmqConsumerService = undefined, // gateway consumer
              fnConsume = undefined) {
    // Gateway consumer
    this.rabbitmqConsumerService?.createAndStartProcessing(fnConsume)
      .then(() => logger.log('Gateway consumer created and started processing'));
  }

  async createWorkflow(bpmnProcessId: string, res: Response = undefined) {
    const sessionId = `${Guid.create()}`;
    try {
      this.workflowInstance = await this.zbClient.createWorkflowInstance(bpmnProcessId, {
        sessionId,
        serviceInstanceId: ServiceInstanceId.getId(),
        tracer: 'init',
      });

      logger.log(`Workflow 1: ${JSON.stringify(this.workflowInstance)}`);

      if (res)
        this.responses.set(sessionId, res);
    }
    catch (err) {
      logger.error(`Error in createWorkflow() for "${bpmnProcessId}": ${err}`);
    }
  }

  protected sendResultToClient(sessionId: string, result: any) {
    const response = this.responses.get(sessionId);
    if (response) {
      response.status(HttpStatus.OK).send(`Workflow result -> ${JSON.stringify(result)}`);
      this.responses.delete(sessionId);
    }
  }

  runTask(job, complete, taskFn, args = null) {
    const result = taskFn(job, complete, args);
    if (result)
      this.sendResultToClient(job.variables.sessionId, result);
  }
}
