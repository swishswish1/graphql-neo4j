import { fetchFromSQLServer } from '../common/utils';
import { ISqlTransaction } from '../modules/sql/sql-interfaces';
import { logger } from '../common/logger';

export class BaseResolver<Sql extends ISqlTransaction, Neo> {
  protected service: any;

  constructor(protected sqlService: Sql, protected neoService: Neo) {
    this.service = fetchFromSQLServer() ? sqlService : neoService;
  }

  public async modifyDb(items: any[], sqlFn: Function, neoFn: Function): Promise<string> {
    const result = await sqlFn(items);
    if (result.isOK) {
      const resultNeo = await neoFn(items);
      result.isOK = resultNeo.isOK;
      result.message += resultNeo.message;
    }
    else
      result.message += 'Neo was not called due to SQL rollback. ';

    await this.sqlService.endTransaction(result);
    return result.message;
  }

  public testPrint(text: string, ...arr: any[]) {
    logger.log(`@@@@@@testPrint: ${text}`);
    for (let i = 0; i < arr.length; i++)
      logger.log(arr[i]);
  }
}
